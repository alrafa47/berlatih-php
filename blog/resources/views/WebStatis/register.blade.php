<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('register.store') }}" method="post">
        @csrf
        <label>First name :</label><br><br>
        <input name="firstName" type="text"><br><br>
        <label>Last name :</label><br><br>
        <input name="lastName" type="text"><br><br>
        <label>Gender</label><br><br>
        <input name="gender" value="Male" type="radio">Male<br>
        <input name="gender" value="Female" type="radio">Female<br>
        <input name="gender" value="Other" type="radio">Other<br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesian">indonesian</option>
            <option value="Australian">Australian</option>
            <option value="Brazilian">Brazilian</option>
            <option value="Argentina">Argentina</option>
            <option value="Italia">Italia</option>
        </select><br>
        <label>Language Spoken</label><br><br>
        <input name="language[]" value="Bahasa" type="checkbox"> Bahasa Indonesia <br>
        <input name="language[]" value="English" type="checkbox"> English <br>
        <input name="language[]" value="Other" type="checkbox"> Other <br>
        <label>Bio:</label><br><br>
        <textarea></textarea><br>
        <input type="submit" value="Sign Up"><br>
    </form>
</body>
</html>
