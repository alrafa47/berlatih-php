@extends('Templates.app')
@section('title', 'Cast')
@section('content-title', 'Cast')
@section('content')
<div class="card">
    <div class="card-body">
        <div>
            <h2>Tambah Data</h2>
                <form action="/cast/store" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" value="{{ old('nama') }}" name="nama" id="nama">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="number" class="form-control" value="{{ old('umur') }}" name="umur" id="umur">
                            @error('umur')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <textarea type="text" class="form-control" value="{{ old('bio') }}" name="bio" id="bio"></textarea>
                            @error('bio')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
        </div>
    </div>
</div>
@endsection
