@extends('Templates.app')
@section('title', 'Cast')
@section('content-title', 'Cast')
@section('content')
<div class="card">
    <div class="card-body">
        <div>
            <h2>Show Data</h2>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <p>{{ $cast->nama }}</p>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <p>{{ $cast->umur }}</p>

                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <p>{{ $cast->bio }}</p>

                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
