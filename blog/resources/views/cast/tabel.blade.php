@extends('Templates.app')
@section('title', 'Cast')
@section('content-title', 'Cast')
@section('content')
<div class="card">
    <div class="card-body">
        <a href="/cast/create" class="btn btn-primary">tambah data</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th style="width: 40px">Bio</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($casts as $cast)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $cast->nama }}</td>
                        <td>{{ $cast->umur }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default" href="/cast/{{ $cast->id }}">Show</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                                <a class="btn btn-warning" href="/cast/{{ $cast->id }}/edit" >Update</a>
                            </div>

                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">Tidak Ada Data</td>
                    </tr>
                @endforelse

            </tbody>
        </table>
    </div>
</div>

@endsection
