<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
    *tugas web statis
*/
// Route::get('/', function () {
//     return view('WebStatis/home');
// });
// Route::name('register.')->group(function () {
//     Route::get('/register', 'AuthController@Index')->name('index');
//     Route::post('/welcome', 'AuthController@Create')->name('store');
// });

/*
    * tugas blade
*/

// Route::get('/', function () {
//     return view('Blade.tabel');
// });
// Route::get('/table', function () {
//     return view('Blade.tabel');
// });
// Route::get('/data-table', function () {
//     return view('Blade.data-tables');
// });


/*
* tugas crud
*/


Route::get('/', 'CastController@index');
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast/store', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
