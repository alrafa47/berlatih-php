<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
    public function Index()
    {
        return view('WebStatis/register');
    }

    public function Create(Request $request)
    {

        $firstName =  $request->firstName;
        $lastName = $request->lastName;
        return view('WebStatis/welcome', compact('firstName', 'lastName'));
    }
}
