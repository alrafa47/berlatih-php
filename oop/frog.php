<?php

require_once("animal.php");
class frog extends animal
{
    public $legs = 4;
    public $cold_blooded = 'no';
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function jump()
    {
        echo "hop hop";
    }
}
