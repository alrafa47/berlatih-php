<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "name : $sheep->name"; // "shaun"
echo '<br>';
echo "legs : $sheep->legs"; // 4
echo '<br>';
echo "cold blooded : $sheep->cold_blooded"; // "no"
echo '<br>';
echo '<br>';


$kodok = new Frog("buduk");
echo "name : $kodok->name"; // "shaun"
echo '<br>';
echo "legs : $kodok->legs"; // 4
echo '<br>';
echo "cold blooded : $kodok->cold_blooded"; // "no"
echo '<br>';
echo "jump : ";
$kodok->jump();
echo '<br>';
echo '<br>';

$sungokong = new Ape("kera sakti");
$sungokong->legs = 2;
echo "name : $sungokong->name"; // "shaun"
echo '<br>';
echo "legs : $sungokong->legs"; // 4
echo '<br>';
echo "cold blooded : $sungokong->cold_blooded"; // "no"
echo '<br>';
echo "yell : "; // "no"
$sungokong->yell();
echo '<br>';
